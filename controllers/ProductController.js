import {ProductModel} from "../models/ProductModel.js";
const getProducts = async (request, response) => {
	try {
		const products = await ProductModel.find();
		response.status(200).json(products);
	} catch (error) {
		response.status(500).json({message: error.message});
	}
}

const getProduct = async (request, response) => {
	try {
		const {id} = request.params;
		const product = await ProductModel.findById(id);
		if (!product) {
			return response.status(404).json(`Product with ID ${id} not found`);
		}
		response.status(200).json(product)
	} catch (error) {
		response.status(500).json({message: error.message});
	}
}

const createProduct = async (request, response) => {
	try {
		const product = await ProductModel.create(request.body);
		response.status(201).json(product);
	} catch (error) {
		response.status(500).json({message: error.message});
	}
}

const updateProduct = async (request, response) => {
	try {
		const {id} = request.params;
		const product = await ProductModel.findByIdAndUpdate(
			{_id: id},
			request.body,
			{new: true}
		);
		response.status(201).json(product);
	} catch (error) {
		response.status(500).json({message: error.message});
	}
}

const deleteProduct = async (request, response) => {
	try {
		const {id} = request.params;
		const product = await ProductModel.findByIdAndDelete(id);
		if (!product) {
			return response.status(404).json(`Product with ID ${id} not found`);
		}
		response.status(200).json("Product successfully remove")
	} catch (error) {
		response.status(500).json({message: error.message});
	}
}


export {getProducts, getProduct, createProduct, updateProduct, deleteProduct}