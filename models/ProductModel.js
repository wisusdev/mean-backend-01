import mongoose from "mongoose";
const {Schema} = mongoose;

// Creando el esquema
const productSchema = new Schema(
	{
		description: {
			type: String,
			required: [true, "Please complete the field"]
		},
		stock: {
			type: Number,
			required: [true, 'Please complete the fiel']
		},
		price: {
			type: Number,
			required: [true, 'Please complete the fiel']
		}
	},
	{
		timestamps:true,
		versionKey:false
	}
);

// Creando el modelo a partir del esquema
export const ProductModel = mongoose.model("Product", productSchema);