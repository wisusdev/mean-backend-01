import express, {Router} from "express";
import {getProducts, getProduct, createProduct, updateProduct, deleteProduct} from "../controllers/ProductController.js";

const router = express.Router();

/*
router.get("/", getProducts);
router.get("/:id", getProduct);
router.post("/", createProduct);
router.put("/:id", updateProduct);
router.delete("/:id", deleteProduct);
*/

router.route('/').get(getProducts).post(createProduct);
router.route('/:id').get(getProduct).put(updateProduct).delete(deleteProduct);


export default router;